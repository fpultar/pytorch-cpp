## Installation

Download and extract **libtorch**: https://pytorch.org/get-started/locally/ (pre-C++ 11 on Linux, Mac OS X doesn't matter)

Get the absolute path to extracted libtorch, configure, and compile. Adopt model as desired.

```bash
cmake -DCMAKE_PREFIX_PATH=/Users/felix/Downloads/libtorch -S src -B build
cmake --build build
build/example-app my_model.pt
```

Execute with: 

```bash
build/example-app my_model.pt
```
