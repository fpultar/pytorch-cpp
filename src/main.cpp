#include <torch/csrc/autograd/function.h>
#include <torch/csrc/autograd/variable.h>
#include <torch/script.h> // One-stop header.

#include <cassert>
#include <iostream>
#include <memory>
#include <tuple>
#include <vector>

int main(int argc, const char *argv[]) {
  if (argc != 2) {
    std::cerr << "usage: example-app <path-to-exported-script-module>\n";
    return -1;
  }

  // read in model
  torch::jit::script::Module module;
  try {
    // Deserialize the ScriptModule from a file using torch::jit::load().
    module = torch::jit::load(argv[1]);
  } catch (const c10::Error &e) {
    std::cerr << "error loading the model\n";
    return -1;
  }

  // C arrays - can be multidimensional but does not have to be (see std::vector
  // example)
  float qm_positions_c[3][5][3] = {
      // 3 frames, 5 atoms, 3 coordinates
      {{3, 4, 2}, {0, 3, 9}, {1, 3, 5}, {1, 2, 5}, {5, 5, 9}},
      {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}, {1, 7, 9}, {2, 2, 2}},
      {{1, 2, 3}, {1, 2, 3}, {1, 2, 3}, {7, 8, 1}, {1, 0, 1}}};

  float mm_positions_c[3][4][3] = {
      // 3 frames, 4 atoms, 3 coordinates
      {{3, 4, 2}, {0, 3, 9}, {1, 3, 5}, {0, 10, 17}},
      {{3, 6, 9}, {10, 45, 0}, {2, 2, 0}, {0, -10, -20}},
      {{1, 2, 3}, {4, 5, 6}, {2, 3, 5}, {0, 1, 1}}};

  // torch::Tensor from C array
  auto options = torch::TensorOptions() // specify data type, where the tensor
                                        // will live, gradients, ...
                                            .dtype(torch::kFloat32)
                                            .device(torch::kCPU)
                                            .layout(torch::kStrided)
                                            .requires_grad(true);
  torch::Tensor qm_positions_tensor =
      torch::from_blob(qm_positions_c, {3, 5, 3},
                       options); // that's how the tensor knows its dimensions
  torch::Tensor mm_positions_tensor =
      torch::from_blob(mm_positions_c, {3, 4, 3}, options);

  // convert to torch::Tensor from std::vector - seems like better approach
  std::vector<float> qm_positions_vector = {
      3, 4, 2, 0, 3, 9, 1, 3, 5, 1, 2, 5, 5, 5, 9, 1, 2, 3, 4, 5, 6, 7, 8,
      9, 1, 7, 9, 2, 2, 2, 1, 2, 3, 1, 2, 3, 1, 2, 3, 7, 8, 1, 1, 0, 1};

  std::vector<float> mm_positions_vector = {
      3, 4, 2, 0, 3,   9,   1, 3, 5, 0, 10, 17, 3, 6, 9, 10, 45, 0,
      2, 2, 0, 0, -10, -20, 1, 2, 3, 4, 5,  6,  2, 3, 5, 0,  1,  1};

  torch::Tensor
      qm_tensor_vector = // expects a void*, specify data type etc. via options
      torch::from_blob(
          qm_positions_vector.data(), {3, 5, 3},
          options); // torch::Tensor is not owning (see memory addresses!)

  torch::Tensor mm_tensor_vector =
      torch::from_blob(mm_positions_vector.data(), {3, 4, 3}, options);

  std::cout << "-----------------------------------------" << std::endl;
  std::cout << "torch::Tensor from std::vector<float> QM:" << std::endl;
  std::cout << "-----------------------------------------" << std::endl;
  std::cout << qm_tensor_vector << std::endl;

  std::cout << "-----------------------------------------" << std::endl;
  std::cout << "torch::Tensor from std::vector<float> MM:" << std::endl;
  std::cout << "-----------------------------------------" << std::endl;
  std::cout << mm_tensor_vector << std::endl;

  // FIRST ROUND: multidimensional C arrays
  // https://github.com/pytorch/pytorch/issues/17165
  // https://discuss.pytorch.org/t/creating-a-tuple-of-tensors-as-input-to-a-traced-model-with-the-c-front-end/37196
  // std::tuple is one solution. Could also be: auto my_tuple =
  // torch::ivalue::Tuple::create({3, 4}); (replace 3 and 4 with tensor1 and
  // tensor2)
  std::tuple<torch::Tensor, torch::Tensor> input_model_c(qm_positions_tensor,
                                                         mm_positions_tensor);
  torch::Tensor energy_c;
  for (size_t i = 0; i < 1; i++) {
    energy_c = module.forward({input_model_c}).toTensor();
    energy_c.backward();
  }

  auto qm_forces_c =
      qm_positions_tensor
          .grad(); // gradient of energy with respect to qm_positions
  auto mm_forces_c =
      mm_positions_tensor
          .grad(); // gradient of energy with respect to mm_positions

  std::cout << "-----------------------------------------" << std::endl;
  std::cout << "Energy - C Array: " << std::endl;
  std::cout << "-----------------------------------------" << std::endl;
  std::cout << energy_c << std::endl;

  std::cout << "-----------------------------------------" << std::endl;
  std::cout << "Forces on QM - C Array: " << std::endl;
  std::cout << "-----------------------------------------" << std::endl;
  std::cout << qm_forces_c << std::endl;

  std::cout << "-----------------------------------------" << std::endl;
  std::cout << "Forces on MM - C Array: " << std::endl;
  std::cout << "-----------------------------------------" << std::endl;
  std::cout << mm_forces_c << std::endl;

  // SECOND ROUND: std::vector<float>
  std::tuple<torch::Tensor, torch::Tensor> input_model_vector(qm_tensor_vector,
                                                              mm_tensor_vector);

  torch::Tensor energy_vector;
  for (size_t i = 0; i < 1; ++i) {
    energy_vector = module.forward({input_model_vector}).toTensor();
    energy_vector.backward();
  }

  auto qm_forces_vector = qm_tensor_vector.grad();
  auto mm_forces_vector = mm_tensor_vector.grad();

  std::cout << "-----------------------------------------" << std::endl;
  std::cout << "Energy - std::vector<float>: " << std::endl;
  std::cout << "-----------------------------------------" << std::endl;
  std::cout << energy_vector << std::endl;

  std::cout << "-----------------------------------------" << std::endl;
  std::cout << "Forces on QM - std::vector<float>: " << std::endl;
  std::cout << "-----------------------------------------" << std::endl;
  std::cout << qm_forces_vector << std::endl;

  std::cout << "-----------------------------------------" << std::endl;
  std::cout << "Forces on MM - std::vector<float>: " << std::endl;
  std::cout << "-----------------------------------------" << std::endl;
  std::cout << mm_forces_vector << std::endl;

  float energy_c_float = energy_c.item<float>();
  float energy_vector_float = energy_vector.item<float>();
  assert((energy_c_float == energy_vector_float) &&
         "Energies are not identical");

  std::cout << "-----------------------------------------" << std::endl;
  std::cout << "Memory addresses:" << std::endl;
  std::cout << "-----------------------------------------" << std::endl;
  std::cout << "Vector lives at: " << qm_positions_vector.data() << std::endl;
  std::cout << "Tensor lives at: " << qm_tensor_vector.data_ptr<float>()
            << std::endl;
  float *first_element_vector = qm_positions_vector.data(); // C array lives here
  float *first_element_tensor = qm_tensor_vector.data_ptr<float>(); // .data is deprecated

  std::cout << "-----------------------------------------" << std::endl;
  std::cout << "First elements:" << std::endl;
  std::cout << "-----------------------------------------" << std::endl;
  std::cout << "First element vector: " << *first_element_vector << std::endl;
  std::cout << "First element tensor: " << *first_element_tensor << std::endl;

  unsigned elements = qm_tensor_vector.numel(); // number of elements in tensor

  std::cout << "-----------------------------------------" << std::endl;
  std::cout << "Printing tensor:" << std::endl;
  std::cout << "-----------------------------------------" << std::endl;
  for (size_t i = 0; i < elements; ++i) {
    std::cout << *first_element_tensor << std::endl;
    first_element_tensor++; // evil pointer arithmetic
  }
  std::cout << "-----------------------------------------" << std::endl;
  std::cout << "Printing vector:" << std::endl;
  std::cout << "-----------------------------------------" << std::endl;
  for (size_t i = 0; i < elements; ++i) {
    std::cout << *first_element_vector << std::endl; // evil pointer arithmetic
    first_element_vector++;
  }
}
